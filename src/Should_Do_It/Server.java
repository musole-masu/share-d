package Should_Do_It;/**
 * Created by winner on 4/20/17.
 */

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Server extends Application {

    private static BorderPane borderPane;
    private static VBox vBoxBp,vBxP,vBxP_1,vBxP_2,sendVbxp,SendvBxP_1,receiveVbxp,ReceivevBxP_1;
    private static HBox hBox,hbox1,hbox2,hBox3,hbox_b1,hBox_b2,sendHbox2,receiveHbox2;
    private static Button btnHome,btnNext,btnBack,btnSend,btnReceive,sendBtn,sendBrowse,saveBtn,btnSwitchoff;
    public static Button AcceptedBtn;
    private static Separator separator;
    private static Pane pane,sendPane,receivePane;
    private static Label label,lSend,lReceive,sendLabel,receiveLabel;
    private static FileChooser fileChooser;
    private static File file;
    public static Image ImageToSend,ImageToReceive;
    public static ImageView ImageToSendView,ImageToReceiveView;
    public static GridPane centerPaneSend,centerPaneReceive;
    public static String ImagePath;
    private static TextArea textFilePath;
    public static ImageView imageView;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //main Frame within which pane and vbox are sat
        borderPane   =   new BorderPane();
        borderPane.setId("borderpane");
        borderPane.getStylesheets().add(getClass().getResource("style.css").toExternalForm());


        //Center Pane with receiving form-----
        receivePane    =   new Pane();
        receivePane.setId("receivePane");
        receivePane.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        receiveVbxp    =   new VBox();
        receiveVbxp.setPadding(new Insets(100,150,15,150));
        receiveVbxp.setId("vBxP");
        receiveVbxp.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        receiveHbox2   =   new HBox();
        receiveLabel   =   new Label("Receive an Image");
        receiveLabel.setFont(Font.font("Ubuntu", FontWeight.BOLD, FontPosture.REGULAR,24));
        receiveLabel.setTextFill(Color.WHITE);
        receiveHbox2.getChildren().add(receiveLabel);

        /*hBox3   =   new HBox(20);
        hBox3.setPadding(new Insets(20,20,20,20));
        hBox3.setId("hBox3");
        hBox3.getStylesheets().add(getClass().getResource("style.css").toExternalForm());*/




        ReceivevBxP_1  =   new VBox(10);
        ReceivevBxP_1.setPadding(new Insets(20,20,20,20));
        ReceivevBxP_1.setId("hBox3");
        ReceivevBxP_1.getStylesheets().add(getClass().getResource("style.css").toExternalForm());













        saveBtn =   new Button("Get the Image");
        saveBtn.setId("sendBtn");
        saveBtn.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        saveBtn.setTextFill(Color.WHITE);
        ButtonHover(saveBtn);
        saveBtn.setOnAction(e->{
            try {
                Receive();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });



        AcceptedBtn =   new Button();
        AcceptedBtn.setId("AcceptedBtn");
        AcceptedBtn.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        AcceptedBtn.setTextFill(Color.WHITE);
        ButtonHover(AcceptedBtn);


        ColumnConstraints constraints1 = new ColumnConstraints();
        constraints1.setHalignment(HPos.RIGHT);
        constraints1.setMinWidth(60);





        centerPaneReceive = new GridPane();
        centerPaneReceive.setHgap(10);
        centerPaneReceive.setVgap(10);
        centerPaneReceive.setPadding(new Insets(10, 10, 10, 10));
        centerPaneReceive.getColumnConstraints()
                .add(constraints1);


        centerPaneReceive.addRow(0,saveBtn);









        receiveVbxp.getChildren().addAll(receiveHbox2,ReceivevBxP_1,centerPaneReceive);






        receivePane.getChildren().add(receiveVbxp);
        borderPane.setCenter(receivePane);


        primaryStage.setTitle("Share_D");
        primaryStage.setScene(new Scene(borderPane, 600, 475));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private void ButtonHover(Button b) {
        b.setOnMouseEntered(e ->{
            b.setScaleX(1.2); b.setScaleY(1.2);
        });
        b.setOnMouseExited(e ->{
            b.setScaleX(1.0); b.setScaleY(1.0);
        });
    }

    public void Receive() throws Exception{
        ServerSocket serverSocket = new ServerSocket(2200);
        Socket socket = serverSocket.accept();
        InputStream inputStream = socket.getInputStream();

        System.out.println("Reading: " + System.currentTimeMillis());

        byte[] sizeAr = new byte[1024];
        inputStream.read(sizeAr);
        int size = ByteBuffer.wrap(sizeAr).asIntBuffer().get();

        byte[] imageAr = new byte[size];
        inputStream.read(imageAr);

        BufferedImage image=null;
        try {
            image = ImageIO.read(new ByteArrayInputStream(imageAr));
        }catch (IOException i){
            System.out.println("Image failed to load");
        }

        WritableImage writableImagee=null;
        if (image!=null){

            writableImagee  =   new WritableImage(image.getWidth(),image.getHeight());
            PixelWriter pixelWriter =   writableImagee.getPixelWriter();

            for (int x=0;x<image.getWidth();x++){
                for (int y=0;y<image.getHeight();y++){
                    pixelWriter.setArgb(x,y,image.getRGB(x,y));
                }
            }

        }

        imageView = new ImageView(writableImagee);
        imageView.setFitHeight(200);
        imageView.setFitWidth(300);
        imageView.setPreserveRatio(true);

        ReceivevBxP_1.getChildren().add(imageView);


    }

}
