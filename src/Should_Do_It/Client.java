package Should_Do_It;/**
 * Created by winner on 4/24/17.
 */

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Client extends Application {

    private static BorderPane borderPane;
    private static VBox vBoxBp,vBxP,vBxP_1,vBxP_2,sendVbxp,SendvBxP_1,receiveVbxp,ReceivevBxP_1;
    private static HBox hBox,hbox1,hbox2,hBox3,hbox_b1,hBox_b2,sendHbox2,receiveHbox2;
    private static Button btnHome,btnNext,btnBack,btnSend,btnReceive,sendBtn,sendBrowse,saveBtn,btnSwitchoff;
    public static Button AcceptedBtn;
    private static Separator separator;
    private static Pane pane,sendPane,receivePane;
    private static Label label,lSend,lReceive,sendLabel,receiveLabel;
    private static FileChooser fileChooser;
    private static File file;
    public static Image ImageToSend,ImageToReceive;
    public static ImageView ImageToSendView,ImageToReceiveView;
    public static GridPane centerPaneSend,centerPaneReceive;
    public static String ImagePath;
    private static TextArea textFilePath;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        //main Frame within which pane and vbox are sat
        borderPane   =   new BorderPane();
        borderPane.setId("borderpane");
        borderPane.getStylesheets().add(getClass().getResource("style.css").toExternalForm());


        //Center Pane with sending form-----
        sendPane    =   new Pane();
        sendPane.setId("sendPane");
        sendPane.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        sendVbxp    =   new VBox();
        sendVbxp.setPadding(new Insets(50,30,50,30));
        sendVbxp.setId("vBxP");
        sendVbxp.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        sendHbox2   =   new HBox();
        sendLabel   =   new Label("Send an Image");
        sendLabel.setFont(Font.font("Ubuntu", FontWeight.BOLD, FontPosture.REGULAR,24));
        sendLabel.setTextFill(Color.WHITE);
        sendHbox2.getChildren().add(sendLabel);

        /*hBox3   =   new HBox(20);
        hBox3.setPadding(new Insets(20,20,20,20));
        hBox3.setId("hBox3");
        hBox3.getStylesheets().add(getClass().getResource("style.css").toExternalForm());*/




        SendvBxP_1  =   new VBox(10);
        SendvBxP_1.setPadding(new Insets(20,20,20,20));
        SendvBxP_1.setId("hBox3");
        SendvBxP_1.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        textFilePath    =   new TextArea();
        textFilePath.setPromptText("Path of the Selected image");
        textFilePath.setPrefSize(150,15);
        textFilePath.setFont(Font.font("Ubuntu", FontWeight.BOLD, FontPosture.REGULAR,12));
        textFilePath.setId("textFilePath");
        textFilePath.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        textFilePath.setEditable(false);

        //FileChooser for getting image we want to send
        fileChooser =   new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image Files","*.jpg","*.gif","*.png")
        );




        sendBrowse=   new Button("Browse");
        sendBrowse.setId("sendBrowse");
        sendBrowse.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        sendBrowse.setTextFill(Color.WHITE);
        ButtonHover(sendBrowse);
        sendBrowse.setOnAction(e->{
            file    =   fileChooser.showOpenDialog(primaryStage);
            if (file!=null){


                ImageToSend =   new Image(file.toURI().toString(),150,150,true,true);
                ImageToSend.isBackgroundLoading();

                ImageToSendView=new ImageView(ImageToSend);
                ImageToSendView.setFitHeight(150);
                ImageToSendView.setFitWidth(150);
                ImageToSendView.setPreserveRatio(true);
                centerPaneSend.addColumn(1,ImageToSendView);
                textFilePath.setText(file.getAbsolutePath());
                ImagePath   =   textFilePath.getText();




            }
        });




        TextField textIP    =   new TextField();
        textIP.setPromptText("Enter the Receiver computer's IP");
        textIP.setFont(Font.font("Ubuntu", FontWeight.BOLD, FontPosture.REGULAR,12));
        textIP.setId("textFilePath");
        textIP.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        sendBtn =   new Button("Send");
        sendBtn.setId("sendBtn");
        sendBtn.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        sendBtn.setTextFill(Color.WHITE);
        ButtonHover(sendBtn);
        sendBtn.setOnAction(e->{

            try {
                Send(ImagePath);

                textFilePath.setText(null);
                ImageToSendView.setImage(null);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });


        ColumnConstraints constraints1 = new ColumnConstraints();
        constraints1.setHalignment(HPos.CENTER);
        constraints1.setMinWidth(60);

        ColumnConstraints constraints2 = new ColumnConstraints();
        constraints2.setHalignment(HPos.CENTER);
        constraints2.setMinWidth(150);

        ColumnConstraints constraints3 = new ColumnConstraints();
        constraints3.setHalignment(HPos.RIGHT);
        constraints3.setMinWidth(60);

        centerPaneSend = new GridPane();
        centerPaneSend.setHgap(10);
        centerPaneSend.setVgap(10);
        centerPaneSend.setPadding(new Insets(10, 10, 10, 10));
        centerPaneSend.getColumnConstraints()
                .addAll(constraints1, constraints2,constraints3);


        centerPaneSend.addRow(0,textFilePath,sendBrowse,textIP);
        centerPaneSend.addColumn(2,sendBtn);






        SendvBxP_1.getChildren().add(centerPaneSend);

        sendVbxp.getChildren().addAll(sendHbox2,SendvBxP_1);






        sendPane.getChildren().add(sendVbxp);
        borderPane.setCenter(sendPane);

        primaryStage.setTitle("Share_D");
        primaryStage.setScene(new Scene(borderPane, 600, 475));
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    private void ButtonHover(Button b) {
        b.setOnMouseEntered(e ->{
            b.setScaleX(1.2); b.setScaleY(1.2);
        });
        b.setOnMouseExited(e ->{
            b.setScaleX(1.0); b.setScaleY(1.0);
        });
    }

    public void Send(String d) throws Exception{
        Socket socket = new Socket("localhost", 2200);
        OutputStream outputStream = socket.getOutputStream();

        BufferedImage image = ImageIO.read(new File(d));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", byteArrayOutputStream);

        byte[] size = ByteBuffer.allocate(1024).putInt(byteArrayOutputStream.size()).array();
        outputStream.write(size);
        outputStream.write(byteArrayOutputStream.toByteArray());
        outputStream.flush();
        System.out.println("Flushed: " + System.currentTimeMillis());

        Thread.sleep(3000);
        System.out.println("Closing: " + System.currentTimeMillis());

    }
}
